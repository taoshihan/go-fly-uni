function replaceMutiFile(str,host){
    return str.replace(/mutiFile\[(.*?)\]/g, function (result) {
        var mutiFiles=result.match(/mutiFile\[(.*?)\]/)
        if (mutiFiles.length<2){
            return result;
        }
        //return result;

        var info=JSON.parse(mutiFiles[1])
        var imgSrc="";
        switch(info.ext){
            case ".mp3":
                imgSrc=host+"/static/images/ext/MP3.png";
                break;
            case ".zip":
                imgSrc=host+"/static/images/ext/ZIP.png";
                break;
            case ".txt":
                imgSrc=host+"/static/images/ext/TXT.png";
                break;
            case ".7z":
                imgSrc=host+"/static/images/ext/7z.png";
                break;
            case ".bpm":
                imgSrc=host+"/static/images/ext/BMP.png";
                break;
            case ".png":
                imgSrc=host+"/static/images/ext/PNG.png";
                break;
            case ".jpg":
                imgSrc=host+"/static/images/ext/JPG.png";
                break;
            case ".jpeg":
                imgSrc=host+"/static/images/ext/JPEG.png";
                break;
            case ".pdf":
                imgSrc=host+"/static/images/ext/PDF.png";
                break;
            case ".doc":
                imgSrc=host+"/static/images/ext/DOC.png";
                break;
            case ".docx":
                imgSrc=host+"/static/images/ext/DOCX.png";
                break;
            case ".rar":
                imgSrc=host+"/static/images/ext/RAR.png";
                break;
            case ".xlsx":
                imgSrc=host+"/static/images/ext/XLSX.png";
                break;
            case ".csv":
                imgSrc=host+"/static/images/ext/XLSX.png";
                break;
            default:
                imgSrc=host+"/static/images/ext/default.png";
                break;
        }
        var img="<img src='"+imgSrc+"' style='width: 38px;height: 38px;' />";
		var path=getImageUrl(info.path,host);
        var html= `
                    <a style="text-decoration: none;display: block;background: #fff;padding: 5px;border-radius: 2px;max-width: 98%;display: flex;" href="`+path+`" target="_blank"/>
                    `+img+`
                    <div style='color: #333;'>
                        <p>`+info.name+`</p>
                        <p style="font-size: 12px;color: #666">`+formatFileSize(info.size)+`</p>
                    </div>
                    </a>
            `;
        return html;

        // return '<a href="'+src+'" target="_blank"/><i style="font-size: 26px;color: #606266" class="el-icon-folder-checked"></i></a>';
    })
}
function formatFileSize(fileSize) {
    if (fileSize < 1024) {
        return fileSize + 'B';
    } else if (fileSize < (1024*1024)) {
        var temp = fileSize / 1024;
        temp = temp.toFixed(2);
        return temp + 'KB';
    } else if (fileSize < (1024*1024*1024)) {
        var temp = fileSize / (1024*1024);
        temp = temp.toFixed(2);
        return temp + 'MB';
    } else {
        var temp = fileSize / (1024*1024*1024);
        temp = temp.toFixed(2);
        return temp + 'GB';
    }
}
/**
 * 人性化时间
 * @param {Object} timestamp
 */
function beautifyTime(timestamp){
    var mistiming = Math.round(new Date() / 1000)-timestamp;
	if(mistiming<=0){
		return "刚刚";
	}
    var postfix = mistiming>0 ? '前' : '后'
    mistiming = Math.abs(mistiming)
    var arrr = ['年','个月','周','天','小时','分钟','秒'];
    var arrn = [31536000,2592000,604800,86400,3600,60,1];
 
    for(var i=0; i<7; i++){
        var inm = Math.floor(mistiming/arrn[i])
        if(inm!=0){
            return inm+arrr[i] + postfix
        }
    }
}
/**
 * 格式化时间
 * @param {Object} time
 * @param {Object} fmt
 */
function formatTime(time, fmt) {
	if (time == null) {
		return;
	}
	var fmt = fmt ? fmt : 'yyyy-MM-dd hh:mm:ss';
	var time = new Date(time);
	var z = {
			M: time.getMonth() + 1, 
			d: time.getDate(), 
			h: time.getHours(),
			m: time.getMinutes(),
			s: time.getSeconds()
		};
	fmt = fmt.replace(/(M+|d+|h+|m+|s+)/g, function(v) {
			return ((v.length > 1 ? "0" : "") + eval('z.' + v.slice(-1))).slice(-2);
		});
	return fmt.replace(/(y+)/g, function(v) {
			return time.getFullYear().toString().slice(-v.length);
		});
}
//js获取当前时间
function getNowDate() {
    var myDate = new Date;
    var year = myDate.getFullYear(); //获取当前年
    var mon = myDate.getMonth() + 1; //获取当前月
    var date = myDate.getDate(); //获取当前日
    var hours = myDate.getHours(); //获取当前小时
    var minutes = myDate.getMinutes(); //获取当前分钟
    var seconds = myDate.getSeconds(); //获取当前秒
    var now = year + "-" + mon + "-" + date + " " + hours + ":" + minutes + ":" + seconds;
    return now;
}
//获取当前时间戳
function getTimestamp() {
    return new Date(getNowDate()).getTime();
}
//获取当前域名
function getBaseUrl() {
    var ishttps = 'https:' == document.location.protocol ? true : false;
    var url = window.location.host;
    if (ishttps) {
        url = 'https://' + url;
    } else {
        url = 'http://' + url;
    }
    return url;
}
//获取ws链接
function getWsUrl(remoteUrl){
	var prefix=remoteUrl.substr(0,5);
	var domain=remoteUrl.replace("http://","").replace("https://","");
	if(prefix=='https'){
		return "wss://"+domain+"/ws_kefu";
	}else{
		return "ws://"+domain+"/ws_kefu";
	}
}
//获取图片链接
function getImageUrl(imgUrl,host){
	var prefix=imgUrl.substr(0,4);
	if(prefix=='http'){
		return imgUrl;
	}else{
		return host+imgUrl;
	}
}
//trim
function trim(str, char) {
  if (char) {
    str=str.replace(new RegExp('^\\'+char+'+|\\'+char+'+$', 'g'), '');
  }
  return str.replace(/^\s+|\s+$/g, '');
};
function emojiGifs(baseUrl){
    var emojiGifs=[];
    var emojiTxt = ["[a]", "[b]", "[c]", "[d]", "[e]", "[f]", "[g]", "[h]", "[i]", "[j]", "[k]", "[l]", "[m]", "[n]", "[o]", "[p]", "[q]", "[r]", "[s]", "[t]", "[u]", "[v]", "[w]", "[x]", "[y]", "[z]", "[aa]", "[bb]", "[cc]", "[dd]", "[ee]", "[ff]", "[gg]", "[hh]", "[ii]", "[jj]", "[kk]", "[ll]", "[mm]", "[nn]", "[oo]", "[pp]", "[qq]", "[rr]"];
    for(var i=0;i<emojiTxt.length;i++){
        emojiGifs[emojiTxt[i]]=baseUrl+"/static/images/face/"+i+".png";
    }
    return emojiGifs;
}
function emojiGifsMap(baseUrl){
    var emojiMap=[];
    var emojis=emojiGifs(baseUrl);
    for(var key in emojis){
        emojiMap.push({"name":key,"path":emojis[key]});
    }
    return emojiMap;
}
function playVoice(){
			var innerAudioContext = uni.createInnerAudioContext();
			innerAudioContext.src = '/static/alert.mp3';
			innerAudioContext.onPlay(() => {
			  console.log('开始播放');
			});
			innerAudioContext.onError((res) => {
			  console.log(res.errMsg);
			  console.log(res.errCode);
			});
			innerAudioContext.play();
			innerAudioContext=null;
		}
//去除html
function replaceHtml(str){
	return str.replace(/<\/?.+?\/?>/g,'');
}
//兼容的getUserMedia
function getCompatibleUserMedia(constraints, successCallback, errorCallback) {
  var media = (navigator.getUserMedia || navigator.webkitGetUserMedia ||navigator.mozGetUserMedia ||navigator.msGetUserMedia);
  
  if (media) {
    media.call(navigator, constraints, successCallback, errorCallback);
	return true;
  } else {
    console.error("Your browser does not support the getUserMedia API.");
	return false;
  }
}
// Base64加密
function b64EncodeUnicode(str) {
    return btoa(encodeURIComponent(str).replace(/%([0-9A-F]{2})/g, function(match, p1) {
        return String.fromCharCode('0x' + p1);
    }));
}
function urlDecode(str) {
  return decodeURIComponent(str.replace(/\%20/g, '+'));
}
// Base64解密
function b64DecodeUnicode(str) {
	str=urlDecode(str);
  return decodeURIComponent(atob(str.replace(/\_/g, '/').replace(/\-/g, '+'))
    .split('')
    .map(function (c) {
      return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
    })
    .join(''));
}
export default {  
	b64DecodeUnicode,
	b64EncodeUnicode,
	getCompatibleUserMedia,
	replaceMutiFile,
	beautifyTime,
	getNowDate,
	playVoice,
	getBaseUrl,
	getWsUrl,
	getImageUrl,
	trim,
	emojiGifsMap,
	replaceHtml
}