//往访客列表增加访客
function addVisitor(visitors,newVisitor){
	if(visitors.length==0){
		visitors=[newVisitor];
		return visitors;
	}
	var flag = false;
	visitors.forEach(function(visitor){
		if(visitor.visitor_id==newVisitor.visitor_id){
			flag=true;
		}
	});
	if (flag) {
		return;
	}
	visitors.unshift(newVisitor);
	return visitors;
}
//从访客列表删除访客
function removeVisitor(visitors,visitorId){
	for (var i = 0; i < visitors.length; i++) {
		if (visitors[i].visitor_id == visitorId) {
			visitors.splice(i, 1);
		}
	}
	return visitors;
}
//接收到一条新消息
function receiveMessage(visitors,message){
	for (var i = 0; i < visitors.length; i++) {
		if (visitors[i].visitor_id == message.visitor_id) {
			visitors[i].last_message = message.content;
			visitors[i].unread_num =parseInt(visitors[i].unread_num)+1;
		}
	}
	return visitors;
}
export default {  
    addVisitor,
	removeVisitor,
	receiveMessage
}